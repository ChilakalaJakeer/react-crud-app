// Allahamdulillah
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { API_BASE_URL } from "../../utils/config";
import Firebase from "../auth/Firebase";
export default class Create extends Component {
  state = {
    productName: "",
    productPrice: 0
  };

  onChangeProductName = e => {
    this.setState({
      productName: e.target.value
    });
    console.log(this.state.productName);
  };
  onChangeProductPrice = e => {
    this.setState({
      productPrice: e.target.value
    });
  };

  onSubmit = () => {
    console.log(" in onSubmit");

    // e.preventDefault();
    const product = {
      productName: this.state.productName,
      productPrice: this.state.productPrice
    };
    axios
      .post(`${API_BASE_URL}/products/create/`, product)
      .then(() => console.log(" prodoct created in mongoDB"))
      .catch(err => {
        console.log(err);
      });

    window.location = "/view";
  };

  render() {
    if (!Firebase.getCurrentUsername()) {
      alert("Please login to create");

      this.props.history.replace("/login");
      return null;
    }
    return (
      <div>
        <h3>
          Create Product{" "}
          <Link to="/view">
            <i
              className="material-icons"
              style={{ float: "right", fontSize: 40 }}
              title="view products"
            >
              view_list
            </i>
          </Link>
        </h3>
        <form>
          <div className="form-group">
            Product Name :
            <input
              type="text"
              name="productName"
              className="form-control"
              value={this.state.productName}
              onChange={this.onChangeProductName}
            />
          </div>
          <div className="form-group">
            Product Price :
            <input
              type="number"
              name="productPrice"
              className="form-control"
              value={this.state.productPrice}
              onChange={this.onChangeProductPrice}
            />
          </div>
          <div className="form-group"></div>
        </form>
        <button
          type="submit"
          className="btn btn-success"
          onClick={this.onSubmit}
          style={{ float: "right" }}
        >
          Submit
        </button>
      </div>
    );
  }
}
