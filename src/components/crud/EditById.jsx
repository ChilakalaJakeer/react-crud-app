import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { API_BASE_URL } from "../../utils/config";
import Firebase from "../auth/Firebase";

export default class Edit extends Component {
  state = {
    productName: "",
    productPrice: 0
  };

  componentDidMount() {
    // allahamdulillah
    axios
      .get(`${API_BASE_URL}/products/` + this.props.match.params.id)
      //mashallah
      .then(response => {
        console.log(
          "allahamdulillah data:",
          response.data,
          "for id:",
          this.props.match.params.id
        );
        this.setState({
          productName: response.data.productName,
          productPrice: response.data.productPrice
        });
      })
      .catch(err => {
        console.log(err, this.props.match.params.id);
      });
    console.log(this.state.products);
  }

  onChangeProductName = e => {
    this.setState({
      productName: e.target.value
    });
    console.log(this.state.productName);
  };
  onChangeProductPrice = e => {
    this.setState({
      productPrice: e.target.value
    });
  };

  onSubmit = () => {
    console.log("Allahamdulillah in onSubmit");

    // e.preventDefault();
    const product = {
      productName: this.state.productName,
      productPrice: this.state.productPrice
    };
    console.log("Allahamdulillah created product is :", product);
    axios
      .put(
        `${API_BASE_URL}/products/update/` + this.props.match.params.id,
        product
      )
      .then(() => console.log("Allahamdulillah product updated to db"))
      .catch(err => {
        console.log(err);
      });

    window.location = "/view";
  };

  render() {
    if (!Firebase.getCurrentUsername()) {
      alert("Please login to edit");
      this.props.history.replace("/loginAuth");
      return null;
    }
    return (
      <div>
        <h3>
          Edit Product
          <Link to="/view">
            <i
              className="material-icons"
              style={{ float: "right", fontSize: 40 }}
              title="view products"
            >
              view_list
            </i>
          </Link>
        </h3>
        <form>
          <div className="form-group">
            Product Name :
            <input
              type="text"
              name="productName"
              className="form-control"
              value={this.state.productName}
              onChange={this.onChangeProductName}
            />
          </div>
          <div className="form-group">
            Product Price :
            <input
              type="number"
              name="productPrice"
              className="form-control"
              value={this.state.productPrice}
              onChange={this.onChangeProductPrice}
            />
          </div>
          <div className="form-group"></div>
        </form>
        <button
          type="submit"
          className="btn btn-success"
          onClick={this.onSubmit}
          style={{ float: "right" }}
        >
          Submit
        </button>
      </div>
    );
  }
}
