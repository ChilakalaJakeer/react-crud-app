import React from "react";
import { Link } from "react-router-dom";
function ViewElement(props) {
  return (
    <tr>
      <td>{props.product.productName}</td>
      {props.product.productPrice === 0 ? (
        <td>Free</td>
      ) : (
        <td>{props.product.productPrice} &#8377;</td>
      )}
      <td>
        <Link to={"/edit/" + props.product._id}>
          <i
            className="material-icons"
            title="edit"
            style={{ paddingRight: 40 }}
          >
            edit
          </i>
        </Link>
        <a
          href=""
          onClick={() => {
            props.deleteProduct(props.product._id);
          }}
        >
          <i class="material-icons" title="delete" style={{ paddingRight: 40 }}>
            delete
          </i>
        </a>
        <Link to={"/product/" + props.product._id}>
          <i className="material-icons">more</i>
        </Link>
      </td>
    </tr>
  );
}

export default ViewElement;
