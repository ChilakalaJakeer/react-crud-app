// allahamdulillah
//mashallah
//subhanallah
import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import socketIO from "socket.io-client";
import ViewElement from "./ViewElement";
import { API_BASE_URL } from "../../utils/config";
import Firebase from "../auth/Firebase";

const socket = socketIO(API_BASE_URL);

export default class View extends Component {
  state = {
    products: []
  };

  componentDidMount() {
    axios
      .get(`${API_BASE_URL}/products/`)
      .then(response => {
        console.log("allahamdulillah data:", response.data);
        this.setState({ products: response.data });
        //socket.io
        socket.emit("products", this.state.products);
      })
      .catch(err => {
        console.log(err);
      });
    socket.on("updatedProducts", data =>
      this.setState({ products: data.updatedProducts })
    );
  }

  deleteProduct = id => {
    if (!Firebase.getCurrentUsername()) {
      alert("Please login to delete");
      this.props.history.replace("/loginAuth");
      return null;
    }
    axios
      .delete(`${API_BASE_URL}/products/delete/` + id)
      .then(response => {
        console.log(response.data);
      })
      .catch(err => {
        console.log(err);
      });
    //updated data after deletion
    this.setState({
      products: this.state.products.filter(el => el._id !== id)
    });
  };

  render() {
    return (
      <div>
        <h3>
          Products List{" "}
          <Link to="/create">
            <i
              className="material-icons"
              style={{ float: "right", fontSize: 40 }}
              title="add product"
            >
              add
            </i>
          </Link>
        </h3>

        <table className="table">
          <thead>
            <tr>
              <th>Product</th>
              <th>Price</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.products.map(product => (
              <ViewElement
                key={product._id}
                product={product}
                // productName={product.productName}
                // productPrice={product.productPrice}
                deleteProduct={this.deleteProduct}
              />
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
