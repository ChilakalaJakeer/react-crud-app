// allahamdulillah
//mashallah
//subhanallah
import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import ViewElement from "./ViewElement";

export default class View extends Component {
  state = {
    product: {
      productName: "",
      productPrice: 0
    }
  };

  componentDidMount() {
    axios
      .get("http://localhost:5000/products/" + this.props.match.params.id)
      .then(response => {
        console.log("allahamdulillah data:", response.data);
        this.setState({ product: response.data });
      })
      .catch(err => {
        console.log(err);
      });
    console.log(this.state.product);
  }

  deleteProduct = id => {
    axios
      .delete("http://localhost:5000/products/delete/" + id)
      .then(response => {
        console.log(response.data);
      })
      .catch(err => {
        console.log(err);
      });
    window.location = "/view";
    //updated data after deletion
    // this.setState({
    //   products: this.state.products.filter(el => el._id !== id)
    // });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Product</th>
              <th>Price</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <ViewElement
              key={this.state.product._id}
              product={this.state.product}
              // productName={product.productName}
              // productPrice={product.productPrice}
              deleteProduct={this.deleteProduct}
            />
          </tbody>
        </table>
      </div>
    );
  }
}
