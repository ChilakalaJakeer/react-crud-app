// Allahamdulillah

import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { API_BASE_URL } from "../config";
export default class Register extends Component {
  state = {
    userName: "",
    email: "",
    password: "",
    password2: ""
  };

  onChangeUserName = e => {
    this.setState({
      userName: e.target.value
    });
    console.log(this.state.userName);
  };
  onChangeEmail = e => {
    this.setState({
      email: e.target.value
    });
  };
  onChangePassword = e => {
    this.setState({
      password: e.target.value
    });
  };
  onChangePassword2 = e => {
    this.setState({
      password2: e.target.value
    });
  };

  onSubmit = () => {
    console.log(" in onSubmit");
    // e.preventDefault();
    const register = {
      userName: this.state.userName,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2
    };
    console.log(" created product is :", register);
    axios
      .post(`${API_BASE_URL}/users/register/`, register)
      .then(response => console.log(response.data))
      .catch(err => {
        console.log(err);
      });

    // window.location = "/view";
  };

  render() {
    return (
      <div>
        <h3>Signup</h3>
        <form>
          <div className="form-group">
            User Name :
            <input
              type="text"
              name="userName"
              className="form-control"
              value={this.state.userName}
              onChange={this.onChangeUserName}
            />
          </div>
          <div className="form-group">
            Email :
            <input
              type="text"
              name="productName"
              className="form-control"
              value={this.state.email}
              onChange={this.onChangeEmail}
            />
          </div>
          <div className="form-group">
            Password:
            <input
              type="text"
              name="password"
              className="form-control"
              value={this.state.password}
              onChange={this.onChangePassword}
            />
          </div>
          <div className="form-group">
            Re-enter password :
            <input
              type="text"
              name="password2"
              className="form-control"
              value={this.state.password2}
              onChange={this.onChangePassword2}
            />
          </div>
        </form>
        <button
          type="submit"
          className="btn btn-success"
          onClick={this.onSubmit}
          style={{ float: "right" }}
        >
          Submit
        </button>

        <span>
          Already have an account then please
          <Link to="/login" className="">
            login
          </Link>
        </span>
      </div>
    );
  }
}
