import React from "react";
import { Link } from "react-router-dom";
import Firebase from "./Firebase";

function Auth(props) {
  async function logout() {
    await Firebase.logout();
    window.location = "/";
  }

  if (!Firebase.getCurrentUsername()) {
    return (
      <li className="nav-item">
        <Link to="/login" className="nav-link">
          Login
        </Link>
      </li>
    );
  } else {
    return (
      <li className="nav-item " onClick={logout}>
        <Link to="/" className="nav-link">
          Logout
        </Link>
      </li>
    );
  }
}
export default Auth;
