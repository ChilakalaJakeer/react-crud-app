import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from "../components/auth/Login";
import Register from "../components/auth/Register";

import Navbar from "../components/Navbar";

import Home from "../components/Home";
import Create from "../components/crud/Create";
import Edit from "../components/crud/EditById";
import View from "../components/crud/View";
import ViewById from "../components/crud/ViewById";
import About from "../components/About";

export default function App() {
  return (
    <Router>
      <Navbar />
      <br />
      <Switch>
        <Route exact path="/" component={Home} />

        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />

        <Route path="/create" component={Create} />
        <Route path="/edit/:id" component={Edit} />
        <Route path="/view" component={View} />
        <Route path="/product/:id" component={ViewById} />

        <Route path="/about" component={About} />
      </Switch>
    </Router>
  );
}
